Title:

SMB3 in Samba - Multi-Channel and beyond

Abstract:

The implementation of SMB3 is a broad and important set of topics on the Samba
roadmap. After a longer period of preparations, the first and the most generally
useful of the advanced SMB3 features has recently arrived in Samba:
Multi-Channel. This presentation will explain Samba's implementation of
Multi-Channel, especially covering the challenges that had to be solved for
integration with Samba's traditional clustering with CTDB, which is invisible to
the SMB clients and hence quite different from the clustering built into SMB3.
Afterwards an outlook will be given on other areas of active development like
persistent file handles, RDMA, and scale-out SMB clustering, reporting on status
and challenges.

Bio:

Michael is one of the main developers of Samba (www.samba.org), specializing on
file serving and clustering. He is an architect and the manager of Red Hat
Storage Server's SMB team.
